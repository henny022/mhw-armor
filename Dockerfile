FROM python:3.7

ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
COPY . /code/

RUN pip install -r requirements.txt

ENTRYPOINT ["python", "manage.py"]

CMD ["runserver", "0.0.0.0:80"]