from django.shortcuts import render
from django.http import HttpResponse, HttpRequest
from django.views import View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.db.utils import IntegrityError

import json


# Create your views here.
def root(request):
    return HttpResponse('MHW Armor api root')


@method_decorator(csrf_exempt, name='dispatch')
class ArrayView(View):
    model = None

    def get(self, request: HttpRequest):
        object = []
        small = 'small' in request.GET
        for o in self.model.objects.all():
            object.append(o.to_obj(small=small))
        return HttpResponse(json.dumps(object), status=200, content_type='application/json')

    def post(self, request):
        try:
            self.model.create(**json.loads(request.body))
            return HttpResponse(status=201)
        except IntegrityError as e:
            object = {'code': 409, 'message': f'{e}'}
            return HttpResponse(json.dumps(object), status=409)


class SingleView(View):
    model = None

    def get(self, request, slug):
        try:
            object = self.model.get(slug).to_obj()
            code = 200
        except self.model.DoesNotExist:
            object = {'code': 404, 'message': f'{self.model.__name__} {slug} not found'}
            code = 404
        return HttpResponse(json.dumps(object), status=code)
