from django.urls import path
from . import views

from core.models import *

urlpatterns = [
    path('', views.root),
    path('skills/', views.ArrayView.as_view(model=Skill)),
    path('skills/<slug:slug>/', views.SingleView.as_view(model=Skill)),
    path('slots/', views.ArrayView.as_view(model=Slot)),
    path('slots/<slug:slug>/', views.SingleView.as_view(model=Slot)),
    path('jewels/', views.ArrayView.as_view(model=Jewel)),
    path('jewels/<slug:slug>/', views.SingleView.as_view(model=Jewel)),
    path('armors/', views.ArrayView.as_view(model=Armor)),
    path('armors/<slug:slug>/', views.SingleView.as_view(model=Armor)),
    path('weapons/', views.ArrayView.as_view(model=Weapon)),
    path('weapons/<slug:slug>/', views.SingleView.as_view(model=Weapon)),
    path('charms/', views.ArrayView.as_view(model=Charm)),
    path('charms/<slug:slug>/', views.SingleView.as_view(model=Charm)),
]
