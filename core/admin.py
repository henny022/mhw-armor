from django.contrib import admin

from . import models

# Register your models here.
admin.site.register(models.Slot)
admin.site.register(models.Skill)
admin.site.register(models.Armor)
admin.site.register(models.Jewel)
admin.site.register(models.ArmorSet)
