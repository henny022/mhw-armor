from django.db import models
from django.contrib import auth
from django.utils.text import slugify

from collections import Counter

# Create your models here.
'''
@startuml

class Weapon {
{abstract} slug: Text
name: Text
attack: Integer
slots: Slot[]
}

class Armor {
{abstract} slug: text
name: Text
skills: Skill[]
defense: Integer
slots: Slot[]
type: ArmorType
}

class Charm {
{abstract} slug: Text
name: Text
skills: Skill[]
}

class Slot {
size: Integer
}

class Skill {
{abstract} slug: Text
name: Text
max_level: Integer
secret_level: Integer
description: Text
}

class Jewel {
{abstract} slug: Text
name; Text
slot: Slot
skills: Skill[]
}

class ArmorSet {
name: Text
owner: User
head: Armor
chest: Armor
arms: Armor
waist: Armor
legs: Armor
weapon: Weapon
charm: Charm
jewels: Jewel[]
}


Armor --> Slot
Armor --> Skill
Jewel --> Skill
Jewel --> Slot
ArmorSet --> Armor
ArmorSet --> Jewel
ArmorSet --> Weapon
ArmorSet --> Charm
Weapon --> Slot
Charm --> Skill

@enduml
'''


# TODO improve this to better handle greek letters, + and /
def create_slug_from_name(name):
    return slugify(name)


def to_obj(model, fields=None, foreign_fields=None, many_fields=None, small=False):
    obj = {}
    if fields:
        for field in fields:
            if small and field is 'slug':
                continue
            obj[field] = getattr(model, field)
    if foreign_fields:
        for field in foreign_fields:
            if small:
                obj[field] = getattr(model, field).to_ref()
            else:
                obj[field] = getattr(model, field).to_obj()
    if many_fields:
        for field in many_fields:
            if small:
                obj[field] = [x.to_ref() for x in getattr(model, field).all()]
            else:
                obj[field] = [x.to_obj() for x in getattr(model, field).all()]
    return obj


class Slot(models.Model):
    size = models.IntegerField()

    @classmethod
    def get(cls, size):
        return cls.objects.get(size=size)

    @classmethod
    def create(cls, size):
        return cls.objects.create(size=size)

    def to_obj(self, small=False):
        if small:
            return {'size': self.size}
        return self.size

    def to_ref(self):
        return self.size

    def __str__(self):
        return f'Slot ({self.size})'


class Skill(models.Model):
    slug = models.TextField(unique=True)
    name = models.TextField()
    max_level = models.IntegerField()
    secret_level = models.IntegerField(blank=True)
    description = models.TextField(blank=True)

    @classmethod
    def create(cls, name, max_level, description, secret_level=None):
        return cls.objects.create(slug=create_slug_from_name(name),
                                  name=name,
                                  max_level=max_level,
                                  secret_level=secret_level or max_level,
                                  description=description)

    @classmethod
    def get(cls, slug):
        return cls.objects.get(slug=slug)

    def to_obj(self, small=False):
        if small:
            if self.secret_level is self.max_level:
                field = ['name', 'max_level', 'description']
            else:
                field = ['name', 'max_level', 'secret_level', 'description']
        else:
            field = ['slug', 'name', 'max_level', 'secret_level', 'description']
        return to_obj(self, field, small=small)

    def to_ref(self):
        return self.slug

    def __str__(self):
        return self.name


class JewelToSkill(models.Model):
    jewel = models.ForeignKey('Jewel', on_delete=models.CASCADE)
    skill = models.ForeignKey(Skill, on_delete=models.PROTECT)


class Jewel(models.Model):
    slug = models.TextField(unique=True)
    name = models.TextField()
    slot = models.ForeignKey(Slot, on_delete=models.PROTECT)
    skills = models.ManyToManyField(Skill, through=JewelToSkill)

    @classmethod
    def create(cls, name, slot: int, skills=None):
        jewel = cls.objects.create(slug=create_slug_from_name(name),
                                   name=name,
                                   slot=Slot.get(slot))
        for skill in skills:
            JewelToSkill.objects.create(jewel=jewel, skill=Skill.get(skill))
        jewel.save()
        return jewel

    @classmethod
    def get(cls, slug):
        return cls.objects.get(slug=slug)

    def to_obj(self, small=False):
        return to_obj(self, ['slug', 'name'], ['slot'], ['skills'], small=small)

    def to_ref(self):
        return self.slug

    def __str__(self):
        return self.name


armor_types = [
    ('head', 'Head'),
    ('chest', 'Chest'),
    ('arms', 'Arms'),
    ('waist', 'Waist'),
    ('legs', 'Legs'),
]


class ArmorToSkill(models.Model):
    armor = models.ForeignKey('Armor', on_delete=models.CASCADE)
    skill = models.ForeignKey(Skill, on_delete=models.PROTECT)


class ArmorToSlot(models.Model):
    armor = models.ForeignKey('Armor', on_delete=models.CASCADE)
    slot = models.ForeignKey(Slot, on_delete=models.PROTECT)


class Armor(models.Model):
    slug = models.TextField(unique=True)
    name = models.TextField()
    skills = models.ManyToManyField(Skill, blank=True, through=ArmorToSkill)
    defense = models.IntegerField()
    slots = models.ManyToManyField(Slot, blank=True, through=ArmorToSlot)
    type = models.CharField(choices=armor_types, max_length=5)

    @classmethod
    def create(cls, name, defense, type, skills=None, slots=None):
        armor = cls.objects.create(slug=create_slug_from_name(name),
                                   name=name,
                                   defense=defense,
                                   type=type)
        for skill in skills:
            ArmorToSkill.objects.create(armor=armor, skill=Skill.get(skill))
        for slot in slots:
            ArmorToSlot.objects.create(armor=armor, slot=Slot.get(slot))
        armor.save()
        return armor

    @classmethod
    def get(cls, slug):
        return cls.objects.get(slug=slug)

    def to_obj(self, small=False):
        return to_obj(self, ['slug', 'name', 'defense', 'type'], many_fields=['skills', 'slots'], small=small)

    def to_ref(self):
        return self.slug

    def __str__(self):
        return self.name


class WeaponToSlot(models.Model):
    weapon = models.ForeignKey('Weapon', on_delete=models.CASCADE)
    slot = models.ForeignKey(Slot, on_delete=models.PROTECT)


class Weapon(models.Model):
    slug = models.TextField(unique=True)
    name = models.TextField()
    attack = models.IntegerField()
    slots = models.ManyToManyField(Slot, blank=True, through=WeaponToSlot)

    @classmethod
    def create(cls, name, attack, slots=None):
        return cls.objects.create(slug=create_slug_from_name(name),
                                  name=name,
                                  attack=attack,
                                  slots=[Slot.get(slot) for slot in slots])

    @classmethod
    def get(cls, slug):
        return cls.objects.get(slug=slug)

    def to_obj(self, small=False):
        return to_obj(self, ['slug', 'name', 'attack'], many_fields=['slots'], small=small)

    def to_ref(self):
        return self.slug

    def __str__(self):
        return self.name


class CharmToSkill(models.Model):
    charm = models.ForeignKey('Charm', on_delete=models.CASCADE)
    skill = models.ForeignKey(Skill, on_delete=models.PROTECT)


class Charm(models.Model):
    slug = models.TextField(unique=True)
    name = models.TextField()
    skills = models.ManyToManyField(Skill, through=CharmToSkill)

    @classmethod
    def create(cls, name, skills=None):
        return cls.objects.create(slug=create_slug_from_name(name),
                                  name=name,
                                  skills=[Skill.get(skill) for skill in skills])

    @classmethod
    def get(cls, slug):
        return cls.objects.get(slug=slug)

    def to_obj(self, small=False):
        return to_obj(self, ['slug', 'name'], many_fields=['skills'], small=small)

    def to_ref(self):
        return self.slug

    def __str__(self):
        return self.name


class ArmorSetToJewel(models.Model):
    armor_set = models.ForeignKey('ArmorSet', on_delete=models.CASCADE)
    jewel = models.ForeignKey(Jewel, on_delete=models.PROTECT)


class ArmorSet(models.Model):
    slug = models.TextField(unique=True)
    name = models.TextField()
    owner = models.ForeignKey(auth.get_user_model(), on_delete=models.CASCADE)
    head = models.ForeignKey(Armor, on_delete=models.PROTECT, related_name='armorset_heads', blank=True, null=True)
    chest = models.ForeignKey(Armor, on_delete=models.PROTECT, related_name='armorset_chests', blank=True, null=True)
    arms = models.ForeignKey(Armor, on_delete=models.PROTECT, related_name='armorset_arms', blank=True, null=True)
    waist = models.ForeignKey(Armor, on_delete=models.PROTECT, related_name='armorset_waists', blank=True, null=True)
    legs = models.ForeignKey(Armor, on_delete=models.PROTECT, related_name='armorset_legs', blank=True, null=True)
    weapon = models.ForeignKey(Weapon, on_delete=models.PROTECT, blank=True, null=True)
    charm = models.ForeignKey(Charm, on_delete=models.PROTECT, blank=True, null=True)
    jewels = models.ManyToManyField(Jewel, blank=True, through=ArmorSetToJewel)

    @classmethod
    def create(cls, name, owner):
        return cls.objects.create(name=name,
                                  owner=owner)

    def get_armor(self):
        return [self.head, self.chest, self.arms, self.waist, self.legs]

    def get_all_skills(self):
        all_skills = []
        for armor in self.get_armor():
            if armor:
                all_skills += armor.skills.all()
        return all_skills

    def get_skills(self):
        return Counter(self.get_all_skills())

    def get_all_slots(self):
        slots = []
        for armor in self.get_armor():
            slots += armor.slots.all()
        return slots

    def get_used_slots(self):
        return [jewel.slot for jewel in self.jewels.all()]

    def get_free_slots(self):
        slots = [] + self.get_all_slots()
        for slot in self.get_used_slots():
            slots.remove(slot)
        return slots

    def to_obj(self, small=False):
        return to_obj(self, ['slug', 'name'], ['head', 'chest', 'arms', 'waist', 'legs', 'weapon', 'charm'], ['jewels'],
                      small=small)

    def to_ref(self):
        return self.slug

    def __str__(self):
        return f'{self.name} by {self.owner.username}'
